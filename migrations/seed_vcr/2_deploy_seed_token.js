const PaymentTokenMockContract = artifacts.require("PaymentTokenMock")
const DaiTokenMockContract = artifacts.require("DaiTokenMock")
const VeVerseSeedTokenContract = artifacts.require("VeVerseSeedToken")

module.exports = async(deployer, network, accounts) => {
    try {
        await deployer.deploy(VeVerseSeedTokenContract)

        if (network !== 'live_mainnet') {
            await deployer.deploy(PaymentTokenMockContract, accounts.length == 1 ? accounts[0] : accounts[1])
            await deployer.deploy(DaiTokenMockContract, accounts.length == 1 ? accounts[0] : accounts[1])
        }
    } catch (err) {
        console.error(err)
    }
}