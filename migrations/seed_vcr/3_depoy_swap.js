const Web3Utils = require('web3-utils');

const SwapContract = artifacts.require("Swap")
const SwapMockContract = artifacts.require("SwapMock")
const PaymentTokenMockContract = artifacts.require("PaymentTokenMock")
const VeVerseSeedTokenContract = artifacts.require("VeVerseSeedToken")

module.exports = async(deployer, network, accounts) => {
    let ADMIN = accounts[0];
    let BENEFICIARY = accounts.length == 1 ? accounts[0] : accounts[5];

    // 18 DECIMALS PAYMENT TOKEN
    //let EXCHANGE_RATE = '2'; // 0.002 with 1000 as EXCHANGE_BASE for a normal 18 decimal token
    //let EXCHANGE_BASE = 1000;
    //let MIN_CONTRIBUTION = web3.utils.toWei('50000', 'ether');
    //let MAX_CONTRIBUTION = web3.utils.toWei('150000', 'ether');

    // USDC
    let EXCHANGE_RATE = '2' // +3 zeros to base for 0.002
    let EXCHANGE_BASE = '1000000000000000' // USDC has 6 decimals + 3 decimals for (0.002 rate), so 18-6+3=15 zeros
    let MIN_CONTRIBUTION = '25000000000' // 25k USDC for initial test
    let MAX_CONTRIBUTION = '200000000000' // 200k USDC

    let PAYMENT_TOKEN; // mutable once
    const VeVerseSeedToken = await VeVerseSeedTokenContract.deployed()
    const VENDOR_TOKEN = VeVerseSeedToken.address

    const MINTER_ROLE = Web3Utils.soliditySha3("MINTER_ROLE")
    const DEFAULT_ADMIN_ROLE = "0x00"
    let KYC_SIGNER = "0x000000000000000000000000a22a6f363610c30bc00c6ff40b290f341fdeec76" // abi encoded 0xa22a6f363610c30bc00c6ff40b290f341fdeec76

    try {
        if (network !== 'live_mainnet') {
            const paymentTokenMockContract = await PaymentTokenMockContract.deployed()
            PAYMENT_TOKEN = paymentTokenMockContract.address
        } else {
            KYC_SIGNER = "0x0000000000000000000000000a22a6f363610c30bc00c6ff40b290f341fdeec76"
            PAYMENT_TOKEN = "0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48" // USDC e.g. https://www.coingecko.com/en/coins/usd-coin
            ADMIN = "0x86578B242243662ccD8000e491d4D51d23bd2a7E" // VeVerse DAO multisig
            BENEFICIARY = "0x86578B242243662ccD8000e491d4D51d23bd2a7E" // VeVerse DAO multisig
            MIN_CONTRIBUTION = '1000000' // 1 USDC for initial test
            MAX_CONTRIBUTION = '200000000000' // 200k USDC
        }

        let swapContract
        if (network === 'develop') {
            swapContract = await deployer.deploy(
                SwapMockContract,
                ADMIN,
                BENEFICIARY,
                VENDOR_TOKEN,
                PAYMENT_TOKEN,
                EXCHANGE_RATE,
                EXCHANGE_BASE,
                MIN_CONTRIBUTION,
                MAX_CONTRIBUTION,
                KYC_SIGNER
            )
        } else {
            swapContract = await deployer.deploy(
                SwapContract,
                ADMIN,
                BENEFICIARY,
                VENDOR_TOKEN,
                PAYMENT_TOKEN,
                EXCHANGE_RATE,
                EXCHANGE_BASE,
                MIN_CONTRIBUTION,
                MAX_CONTRIBUTION,
                KYC_SIGNER
            )
        }

        await VeVerseSeedToken.grantRole(MINTER_ROLE, swapContract.address)
        await VeVerseSeedToken.grantRole(MINTER_ROLE, BENEFICIARY)
        await VeVerseSeedToken.grantRole(DEFAULT_ADMIN_ROLE, ADMIN)

        if (network === 'live_mainnet') {
            await VeVerseSeedToken.revokeRole(MINTER_ROLE, accounts[0])
            await VeVerseSeedToken.revokeRole(DEFAULT_ADMIN_ROLE, accounts[0])
        }
    } catch (err) {
        console.error(err)
    }
}