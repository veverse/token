const Web3Utils = require('web3-utils');

const VeVerseTokenContract = artifacts.require("VeVerseToken")

const MINTER_ROLE = Web3Utils.soliditySha3("MINTER_ROLE")
const DEFAULT_ADMIN_ROLE = "0x00"
const TOTAL_SUPPLY = web3.utils.toWei('1000000000', 'ether');

module.exports = async(deployer, network, accounts) => {
    try {
        await deployer.deploy(VeVerseTokenContract)
        const VeVerseToken = await VeVerseTokenContract.deployed()

        if (network !== 'live_mainnet') {
            await VeVerseToken.grantRole(MINTER_ROLE, accounts[0])
            await VeVerseToken.mint(accounts[0], TOTAL_SUPPLY)
        }

        if (network === 'live_mainnet') {
            await VeVerseToken.revokeRole(MINTER_ROLE, accounts[0])
            await VeVerseToken.revokeRole(DEFAULT_ADMIN_ROLE, accounts[0])
        }
    } catch (err) {
        console.error(err)
    }
}