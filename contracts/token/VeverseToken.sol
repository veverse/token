// SPDX-License-Identifier: MPL-2.0

pragma solidity 0.8.10;

import "../../node_modules/@openzeppelin/contracts/token/ERC20/presets/ERC20PresetMinterPauser.sol";

/** 
 * @title VeVerse ecosystem token.
 *
 * @dev used for staking rewards, minting NFTs, LP and other activity. 
 *
 * Managed (mint, burn, pause) by VeVerse DAO multisig.
 */
contract VeVerseToken is ERC20PresetMinterPauser {
    /** 
     * @dev Setup VCR token.
     */
    constructor() ERC20PresetMinterPauser("VeVerse token", "VCR") {
    }
}