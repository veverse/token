# VeVerse token smart contracts

## token/VeVerseSeedToken.sol
ERC-20 token with custom ERC-1404 implementation to limit transfers to whitelisted contract address, which will be used for the future conversion to VeVerse ecosystem token (VCR).

## Swap.sol
Mints ERC-20 seed tokens for payment token contribution (USDC) by a fixed exchange rate. All contributed tokens go straight to [VeVerse DAO multisig](https://gnosis-safe.io/app/#/safes/eth:0x86578B242243662ccD8000e491d4D51d23bd2a7E/balances).

Uses whitelisting businesses logic for participation, inspired by [Gnosis Auction code](https://github.com/gnosis/ido-contracts/blob/main/contracts/allowListExamples/AllowListOffChainManaged.sol) and the [following article](https://medium.com/@PhABC/off-chain-whitelist-with-on-chain-verification-for-ethereum-smart-contracts-1563ca4b8f11).

# Local installation
Expects Ganache or similar Ethereum client on `127.0.0.1:8545`, default `network_id` is 5777.

    npm install
    npm run migrate:dev
    npm run migrate:dev:vcr_seed
    npm run migrate:dev:vcr

# Testing
Signatures for whitelisting can be generated with the following cli command: `truffle exec tasks/generate_signatures.js --network=develop --file_with_address="../your_address_inputs.txt"`

Default KYC signer wallet used for tests is `0x4253e41227F7af597176963A4F1e8399539e60D5`, it's derived from `nerve panda scare join quantum reform ski umbrella clap worth barely ask` test mnemonic. _Don't use it for anything other than local tests, it's insecure._

`tasks/generate_signatures.js` you can replace test mnemonic with your local Ganache mnemonic, in such case first wallet `provider.getAddress(0)` will be a whitelisting KYC signer.

You may need to update `CLIENT_WHITELIST_SIGNATURE` and `CLIENT_NO_MONEY_WHITELIST_SIGNATURE` in `test/seed_token.js` and `KYC_SIGNER` in `migrations/2_deploy_seed_token.js` for a local signer wallet. Alternatively `KYC_SIGNER` can be updated on a smart contract level, like: `swap.updateContractData.sendTransaction("YOUR_ABI_ENCODED_KYC_SIGNER_WALLET", ether("50000"), ether("150000"), { from: ADMIN })`.

To run automatic tests: 

    npm run test

# Deployments

## Mainnet

* VeVerseSeedToken: 0x08E89A5f292398cc1581655592E2E6C9c11Ab272
* Swap: 0x9E7baB365BcA758681c6ee44bc38BFAf121B6a7d

## Rinkeby

* VeVerseToken: 0xE6eCE7cCaB7982930C9fa0bf04f460AFb2408274
* VeVerseSeedToken: 0x965ad1178D83D988d68d591c8E2Dc89127c60805
* Swap: 0x95cb04c2Ab1d834Ef22B6b2A60561a3984319AA3
* PaymentTokenMock: 0xc955EBe03820e2E063BbFB4146bb7fcfDEE06CA0
* DaiTokenMock: 0x2202524B796C73c328b99D8A6Eb2d2712e4bA00a

# Direct interactions with smart contract by DAO

1) Provide address which will call smart contract to Greet team and get confirmation that it's whitelisted and `SIGNATURE` value;
2) Send `approve(0x9E7baB365BcA758681c6ee44bc38BFAf121B6a7d, 250000000000)` transaction to `0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48`;
3) Send `contribute(250000000000, 0xd183d7aadc13ca77a0189b5355e86039be51773704ce672f08afbfb1527400e9, SIGNATURE)` transaction to `0x9E7baB365BcA758681c6ee44bc38BFAf121B6a7d`;
4) Check ERC20 balance of seedVCR for `0x08E89A5f292398cc1581655592E2E6C9c11Ab272` token address.