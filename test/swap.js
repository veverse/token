const { assert } = require('chai');
const truffleAssert = require('truffle-assertions');

const SwapMock = artifacts.require('SwapMock');
const PaymentTokenMock = artifacts.require('PaymentTokenMock');
const DaiTokenMock = artifacts.require('DaiTokenMock');
const VeVerseSeedToken = artifacts.require('VeVerseSeedToken');

const SIGNER = "0x000000000000000000000000611d8ca43d3e53b6216a2dc25baf23d753867f16"
const CLIENT_WHITELIST_SIGNATURE = "0x000000000000000000000000000000000000000000000000000000000000001b4831ee1bc74533e51a203463345e8a68948fe44f191e013636e333fca6d862eb240dc6e6ef2866e551d5a605ce9016d8f49062671b1bac49e6e2495834d02617"
const WRONG_SIGNATURE = "0x000000000000000000000000000000000000000000000000000000000000001b20dbf31fc19576bd59078b5e0e0ff10d0a6a6d1d3d5358db07d1beaa3ede9f071b0446e8def440bae97587ea7b21956adce69d8c87e1a8e7eb02b34b967829aa"
const CLIENT_NO_USDC_WHITELIST_SIGNATURE = "0x000000000000000000000000000000000000000000000000000000000000001c51b9179c14f13b89d1151a1e0919f0b1f08dc859822fc0a2d73618995bea6677561e5232f3386030ad06130aa96ab188697d53e176a34a9b046b44d51b8572cd"
const REFERENCE = "0x0349c8397e5b9f1c2b59c8ccc0f89f99d8d1a082fc36607bddf91ed7549b2552"

const DEFAULT_CONTRIBUTION = '50000000000'

function ether(value) {
    return web3.utils.toWei(value, 'ether');
}

function usdc(value) {
    return `${value}000000`
}

// State persists sequencially for each test
contract('Swap', accounts => {
    const ADMIN = accounts[0]
    const BENEFICIARY = accounts[5]
    const CLIENT = accounts[1]
    const RANDOM = accounts[2]
    const CLIENT_NO_USDC = accounts[3]

    it('should have valid exchange rate', async() => {
        const swap = await SwapMock.deployed()
        const estimate = await swap.getVendorTokenAmount.call('111111111111', { from: CLIENT }) // 111k usdc with overflow
        assert.equal(
            estimate.toString(),
            ether("55555555.555"),
            "Invalid amount"
        );
    })

    it('should throw for negative amount', async() => {
        const swap = await SwapMock.deployed()
        await truffleAssert.fails(
            swap.getVendorTokenAmount.call("-1", { from: CLIENT }),
            null,
            "value out-of-bounds"
        );
    })

    it('should be pausible', async() => {
        const swap = await SwapMock.deployed()
        assert.equal(await swap.paused.call({ from: CLIENT }), false, "already paused")
        await swap.pause.sendTransaction({ from: ADMIN })
        assert.equal(await swap.paused.call({ from: CLIENT }), true, "not paused")

        await truffleAssert.fails(
            swap.contribute.sendTransaction(DEFAULT_CONTRIBUTION, REFERENCE, CLIENT_WHITELIST_SIGNATURE, { from: CLIENT }),
            null,
            "Pausable: paused"
        );

        await swap.unpause.sendTransaction({ from: ADMIN })
        assert.equal(await swap.paused.call({ from: CLIENT }), false, "still paused")
    })

    it('should not allow to pause without PAUSER_ROLE', async() => {
        const swap = await SwapMock.deployed()
        await truffleAssert.fails(
            swap.pause.sendTransaction({ from: CLIENT }),
            null,
            "must have pauser role"
        );
    })

    it('should not allow the change of payment token by non-admin', async() => {
        const swap = await SwapMock.deployed()
        const paymentToken = await DaiTokenMock.deployed()
        await truffleAssert.fails(
            swap.changePaymentToken.sendTransaction(paymentToken.address, 35, 1000, ether('50000'), ether('150000'), { from: CLIENT }),
            null,
            "must have admin role"
        );
    })

    it('should allow change of KYC signer by admin', async() => {
        const swap = await SwapMock.deployed()
        await swap.updateKycSigner.sendTransaction("0x00", { from: ADMIN })
        assert.equal(await swap.kycSignerData.call({ from: CLIENT }), "0x00", "not changed")
        await swap.updateKycSigner.sendTransaction(SIGNER, { from: ADMIN })
    })

    it('should not allow change of KYC signer by non-admin', async() => {
        const swap = await SwapMock.deployed()
        await truffleAssert.fails(
            swap.updateKycSigner.sendTransaction("0x00", { from: CLIENT }),
            null,
            "must have admin role"
        );
    })

    it('should allow contribution from whitelisted wallet', async() => {
        const swap = await SwapMock.deployed()
        const result = await swap.isAllowed.call(CLIENT, CLIENT_WHITELIST_SIGNATURE, { from: CLIENT })
        assert.equal(result, "0xe3f756de", "should be allowed")
    })

    it('should not allow contribution from random wallet', async() => {
        const swap = await SwapMock.deployed()
        const result = await swap.isAllowed.call(RANDOM, CLIENT_WHITELIST_SIGNATURE, { from: CLIENT })
        assert.equal(result, "0x00000000", "shouldn't be allowed")
    })

    it('should not allow contribution with a wrong signature', async() => {
        const swap = await SwapMock.deployed()
        const paymentToken = await PaymentTokenMock.deployed()
        await paymentToken.approve.sendTransaction(swap.address, DEFAULT_CONTRIBUTION, { from: CLIENT })
        await truffleAssert.fails(
            swap.contribute.sendTransaction(DEFAULT_CONTRIBUTION, REFERENCE, WRONG_SIGNATURE, { from: CLIENT }),
            null,
            "not whitelisted wallet"
        )
    })

    it('should perform contribution from whitelisted wallet', async() => {
        const swap = await SwapMock.deployed()
        const seedToken = await VeVerseSeedToken.deployed()
        const paymentToken = await PaymentTokenMock.deployed()
        const balance = await paymentToken.balanceOf.call(CLIENT, { from: CLIENT })
        const benBalance = await paymentToken.balanceOf.call(BENEFICIARY, { from: BENEFICIARY })
        await paymentToken.approve.sendTransaction(swap.address, DEFAULT_CONTRIBUTION, { from: CLIENT })
        await swap.contribute.sendTransaction(DEFAULT_CONTRIBUTION, REFERENCE, CLIENT_WHITELIST_SIGNATURE, { from: CLIENT })
        const contributed = await seedToken.balanceOf.call(CLIENT, { from: CLIENT })
        assert.equal(contributed.toString(), "25000000000000000000000000", "invalid grant")
        const newBalance = await paymentToken.balanceOf.call(CLIENT, { from: CLIENT })
        assert.equal(balance.sub(newBalance).toString(), DEFAULT_CONTRIBUTION, "invalid payment")
        const benNewBalance = await paymentToken.balanceOf.call(BENEFICIARY, { from: BENEFICIARY })
        assert.equal(benNewBalance.sub(benBalance).toString(), DEFAULT_CONTRIBUTION, "invalid payment")
    })

    it('should allow contribution in several transactions from whitelisted wallet', async() => {
        const swap = await SwapMock.deployed()
        const seedToken = await VeVerseSeedToken.deployed()
        const paymentToken = await PaymentTokenMock.deployed()
        const balance = await paymentToken.balanceOf.call(CLIENT, { from: CLIENT })
        const contribution = usdc("150000")
        await paymentToken.approve.sendTransaction(swap.address, contribution, { from: CLIENT })
        await swap.contribute.sendTransaction(contribution, REFERENCE, CLIENT_WHITELIST_SIGNATURE, { from: CLIENT })
        const contributed = await seedToken.balanceOf.call(CLIENT, { from: CLIENT })
        assert.equal(contributed.toString(), "100000000000000000000000000", "invalid grant")
        const newBalance = await paymentToken.balanceOf.call(CLIENT, { from: CLIENT })
        assert.equal(balance.sub(newBalance).toString(), usdc("150000"), "invalid payment")
    })

    it('should not allow contribution to exceed the limit in a several transactions from whitelisted wallet', async() => {
        const swap = await SwapMock.deployed()
        const paymentToken = await PaymentTokenMock.deployed()
        const contribution = DEFAULT_CONTRIBUTION
        await paymentToken.approve.sendTransaction(swap.address, contribution, { from: CLIENT })
        await truffleAssert.fails(
            swap.contribute.sendTransaction(contribution, REFERENCE, CLIENT_WHITELIST_SIGNATURE, { from: CLIENT }),
            null,
            "invalid amount"
        )
    })

    it('should not allow contribution for less than 25k from whitelisted wallet', async() => {
        const swap = await SwapMock.deployed()
        const paymentToken = await PaymentTokenMock.deployed()
        const contribution = usdc("24999")
        await paymentToken.approve.sendTransaction(swap.address, contribution, { from: CLIENT })
        await truffleAssert.fails(
            swap.contribute.sendTransaction(contribution, REFERENCE, CLIENT_WHITELIST_SIGNATURE, { from: CLIENT }),
            null,
            "invalid amount"
        )
    })

    it('should not allow contribution for more than 200k from whitelisted wallet', async() => {
        const swap = await SwapMock.deployed()
        const paymentToken = await PaymentTokenMock.deployed()
        const contribution = usdc("200001")
        await paymentToken.approve.sendTransaction(swap.address, contribution, { from: CLIENT })
        await truffleAssert.fails(
            swap.contribute.sendTransaction(contribution, REFERENCE, CLIENT_WHITELIST_SIGNATURE, { from: CLIENT }),
            null,
            "invalid amount"
        )
    })

    it('should not allow contribution from unlisted wallet', async() => {
        const swap = await SwapMock.deployed()
        const paymentToken = await PaymentTokenMock.deployed()
        const contribution = usdc("50000")
        await paymentToken.faucet.sendTransaction({ from: RANDOM })
        await paymentToken.approve.sendTransaction(swap.address, contribution, { from: RANDOM })
        await truffleAssert.fails(
            swap.contribute.sendTransaction(contribution, REFERENCE, CLIENT_WHITELIST_SIGNATURE, { from: RANDOM }),
            null,
            "not whitelisted wallet"
        )
    })

    it('should not allow contribution from a whitelisted wallet without payment token amount', async() => {
        const swap = await SwapMock.deployed()
        const paymentToken = await PaymentTokenMock.deployed()
        const balance = await paymentToken.balanceOf.call(CLIENT_NO_USDC, { from: CLIENT_NO_USDC })
        assert.equal(balance.toString(), "0", "has money")
        const contribution = usdc("50000")
        await paymentToken.approve.sendTransaction(swap.address, contribution, { from: CLIENT_NO_USDC })
        await truffleAssert.fails(
            swap.contribute.sendTransaction(contribution, REFERENCE, CLIENT_NO_USDC_WHITELIST_SIGNATURE, { from: CLIENT_NO_USDC }),
            null,
            "ERC20: transfer amount exceeds balance"
        )
    })

    it('should allow the change of payment token', async() => {
        const swap = await SwapMock.deployed()
        const paymentToken = await DaiTokenMock.deployed()
        await swap.changePaymentToken.sendTransaction(paymentToken.address, 35, 1000, ether('10000'), ether('100000'), { from: ADMIN })
        assert.equal(await swap.minContribution.call({ from: CLIENT }).then((i) => i.toString()), ether('10000'), "not changed")
        assert.equal(await swap.maxContribution.call({ from: CLIENT }).then((i) => i.toString()), ether('100000'), "not changed")
        await paymentToken.faucet.sendTransaction({ from: CLIENT_NO_USDC })

        const seedToken = await VeVerseSeedToken.deployed()
        const balance = await paymentToken.balanceOf.call(CLIENT_NO_USDC, { from: CLIENT_NO_USDC })
        const benBalance = await paymentToken.balanceOf.call(BENEFICIARY, { from: BENEFICIARY })
        await paymentToken.approve.sendTransaction(swap.address, ether('50000'), { from: CLIENT_NO_USDC })
        await swap.contribute.sendTransaction(ether('50000'), REFERENCE, CLIENT_NO_USDC_WHITELIST_SIGNATURE, { from: CLIENT_NO_USDC })
        const contributed = await seedToken.balanceOf.call(CLIENT_NO_USDC, { from: CLIENT_NO_USDC })
        assert.equal(contributed.toString(), "1428571428571428571428000", "invalid grant")
        const newBalance = await paymentToken.balanceOf.call(CLIENT_NO_USDC, { from: CLIENT_NO_USDC })
        assert.equal(balance.sub(newBalance).toString(), ether('50000'), "invalid payment")
        const benNewBalance = await paymentToken.balanceOf.call(BENEFICIARY, { from: BENEFICIARY })
        assert.equal(benNewBalance.sub(benBalance).toString(), ether('50000'), "invalid payment")

        const oldPaymentToken = await PaymentTokenMock.deployed()
        await swap.changePaymentToken.sendTransaction(oldPaymentToken.address, 35, '1000000000000000', usdc("50000"), usdc("150000"), { from: ADMIN })
    })
})